<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($tempDB = null)
    {  
        if($tempDB == null)
        {
            $pwd = "";
            $user = "root";
    
            $tempDB = new PDO('mysql:host=localhost;dbname=test;charset=utf8', $user, $pwd);
            
            $tempDB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }

        $this->db = $tempDB;
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
        try
        {
            $booklist = array();
        
            $stmt = $this->db->prepare("SELECT * FROM book ORDER BY id ASC");
            $stmt->execute();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
            {
                $b = new Book($row["title"], $row["author"], $row["description"], $row["id"]);
    
                array_push($booklist, $b);
            }

            return $booklist;
        }
        catch(PDOException $e)
        {
            // There's a problem I've been unable to resolve,
            // when the table is empty I will always get a PDOException
            // and therefore an error page will always be returned.
            // The only solution seems to be to not use try-catch
            return null;
        }
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
        $b = null;

        if(is_numeric($id))
        {
            $booklist = $this->getBookList();

            foreach($booklist as $book)
            {
                if ($book->id == $id)
                {
                    $b = $book;
                }
            }
            return $b;
        }
        else
        {
            return null;
        }
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
        if ($book->title != "" && $book->author != "")
        {
            try
            {
                $stmt = $this->db->prepare
                (
                    "INSERT INTO book (Title, Author, Description) 
                    VALUES (?, ?, ?)"
                );
                $stmt->bindValue(1, $book->title, PDO::PARAM_STR);
                $stmt->bindValue(2, $book->author, PDO::PARAM_STR);
                $stmt->bindValue(3, $book->description, PDO::PARAM_STR);
                $stmt->execute();

                $book->id = $this->db->lastInsertId();
                return true;
            }
            catch(PDOException $e)
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
        if ($book->id > 0 && $book->title != "" && $book->author != "")
        {
            try
            {
                $stmt = $this->db->prepare
                (
                    "UPDATE book SET title=?, author=?, description=? WHERE id=?"
                );
                $stmt->bindValue(1, $book->title, PDO::PARAM_STR);
                $stmt->bindValue(2, $book->author, PDO::PARAM_STR);
                $stmt->bindValue(3, $book->description, PDO::PARAM_STR);
                $stmt->bindValue(4, $book->id, PDO::PARAM_INT);

                $stmt->execute();
                return true;
            }
            catch(PDOException $e)
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
        if (is_numeric($id))
        {
            try
            {
                $stmt = $this->db->prepare
                (
                    "DELETE FROM book WHERE id=?"  
                );
                $stmt->bindValue(1, $id, PDO::PARAM_INT);
                $stmt->execute();

                return true;
            }
            catch(PDOException $e)
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }
}
?>